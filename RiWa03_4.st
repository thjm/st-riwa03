(*
   Project: RiWa03_4 - 20140508
   
   Author:  HJM
   
   Features: - Steuerung fuer Kellerabgang (2 Taster & 2 Lampen)
             - Wohnzimmerlicht (2 Taster (Rest parallel) & 2 Lampen)
             - Diagnostik f�r Einspeiseklemme 24V
             - 2 Lampen auf Terasse
             - F_IS_ADVENT() & F_IS_NIGHT()
             - SET_RTC()
             - 1 mit auto-off tags�ber
             - Steckdosen �ber Taster
             - Steckdose mit Zeituhr
             - Rolladen (Fenster) �ber Taster, Nachtschaltung
             - Rolladen (T�r) �ber Taster
   
   $Id: RiWa03_4.st 19389 2014-10-15 20:57:38Z mathes $
 *)

TYPE ST_SPSInVariables :
STRUCT
	(* --- KL1104 an Position 2, 2 Koppelrelais --- *)

	bTASTER_EG1 AT %IX0.0 : BOOL;
	bTASTER_KG AT %IX0.1 : BOOL;
	bTASTER_EG2 AT %IX0.2 : BOOL;
	bIN_0_3 AT %IX0.3 : BOOL;

	(* --- KL1104 an Position 3, 2 Koppelrelais --- *)

	bTASTER_P1 AT %IX0.4 : BOOL;
	bTASTER_P2 AT %IX0.5 : BOOL;
	bTASTER_P3 AT %IX0.6 : BOOL;
	bTASTER_P4 AT %IX0.7 : BOOL;

	(* --- KL1104 an Position 4 --- *)

	bTASTER_S1 AT %IX1.0 : BOOL;
	bTASTER_S2 AT %IX1.1 : BOOL;
	bTASTER_S3 AT %IX1.2 : BOOL;
	bTASTER_S4 AT %IX1.3 : BOOL;

	(* --- KL1104 an Position 5 --- *)

	bIN_1_4 AT %IX1.4 : BOOL;
	bIN_1_5 AT %IX1.5 : BOOL;
	bTASTER_T1 AT %IX1.6 : BOOL;
	bTASTER_T2 AT %IX1.7 : BOOL;
	
	(* --- KL1104 an Position 6 --- *)

	bTASTER_G1 AT %IX2.0 : BOOL;
	bTASTER_G2 AT %IX2.1 : BOOL;
	bTASTER_G3 AT %IX2.2 : BOOL;
	bTASTER_G4 AT %IX2.3 : BOOL;

	(* --- KL1104 an Position 7 --- *)

	bIN_2_4 AT %IX2.4 : BOOL;
	bIN_2_5 AT %IX2.5 : BOOL;
	bIN_2_6 AT %IX2.6 : BOOL;
	bIN_2_7 AT %IX2.7 : BOOL;

(* --- 750-609 (mit Diagnostik) an Position 8 --- *)

	b24VPowerGood AT %IX3.0 : BOOL;
	b24VFuseFail AT %IX3.1 : BOOL;

(* --- KL1702 / 750-405 an Position 19 --- *)

	bTASTER_BLIND_WINDOW_UP AT %IX3.2 : BOOL;
	bTASTER_BLIND_WINDOW_DOWN AT %IX3.3 : BOOL;

(* --- KL1702 / 750-405 an Position 21 --- *)

	bTASTER_BLIND_DOOR_UP AT %IX3.4 : BOOL;
	bTASTER_BLIND_DOOR_DOWN AT %IX3.5 : BOOL;

END_STRUCT
END_TYPE

TYPE ST_SPSoutVariables :
STRUCT
(* --- KL2134 an Position 9 --- *)

	bSteckdose1 AT %QX0.0 : BOOL;
	bSteckdose2 AT %QX0.1 : BOOL;
	bSteckdose3 AT %QX0.2 : BOOL;
	bSteckdose4 AT %QX0.3 : BOOL;

(* --- KL2134 an Position 10 --- *)

	bSteckdose9 AT %QX0.4 : BOOL;
	bSteckdose10 AT %QX0.5 : BOOL;
	bSteckdose7 AT %QX0.6 : BOOL;
	bSteckdose8 AT %QX0.7 : BOOL;

(* --- KL2134 an Position 11 --- *)

	bSteckdose5 AT %QX1.0 : BOOL;
	bSteckdose6 AT %QX1.1 : BOOL;
	bSteckdose11 AT %QX1.2 : BOOL;
	bOUT_1_3 AT %QX1.3 : BOOL;

(* --- Tbd: KL6180 (Trennklemme) an Position 12 --- *)

	(* --- KL9150 ... / 750-611 an Position 13 --- *)

	(* --- KL2602 / 750-512 an Position 14 --- *)
	bLampe_EG AT %QX1.4 : BOOL;
	bLampe_KG AT %QX1.5 : BOOL;

(* --- KL9020 (K-Bus-Verl�ngerung) an Position 15 --- *)
(* --- KL9050 (K-Bus ??) an Position 16 --- *)

	(* --- KL9150 ... / 750-611 an Position 15 --- *)

	(* --- KL2602 / 750-512 an Position 16 --- *)
	bLampe1 AT %QX1.6 : BOOL;
	bLampe2 AT %QX1.7 : BOOL;

	(* --- KL2602 / 750-512 an Position 17 --- *)
	bLampe_T1 AT %QX2.0 : BOOL;
	bLampe_T2 AT %QX2.1 : BOOL;

	(* --- KL2602 / KL2622 / 750-512 an Position 18 --- *)
	bMOTOR_BLIND_WINDOW_UP AT %QX2.2 : BOOL;
	bMOTOR_BLIND_WINDOW_DOWN AT %QX2.3 : BOOL;

	(* --- KL2602 / KL2622 / 750-512 an Positn 20 --- *)
	bMOTOR_BLIND_DOOR_UP AT %QX2.4 : BOOL;
	bMOTOR_BLIND_DOOR_DOWN AT %QX2.5 : BOOL;

(* --- KL9010 (Endklemme) an Position 20 --- *)

END_STRUCT
END_TYPE

VAR_GLOBAL
	setup : CONSTANTS_SETUP;

	stSPSinVar AT %IB0 : ST_SPSinVariables;
	stSPSoutVar AT %QB0 : ST_SPSoutVariables;
END_VAR

FUNCTION F_IS_ADVENT : BOOL
VAR_INPUT
	dtCurrent : DATE_AND_TIME;
END_VAR
VAR
	dtToday : DATE;
	iDay : INT;
END_VAR

(* Setze bADVENT im Dezember und bis zum 15. Januar *)

dtToday := DT_TO_DATE(dtCurrent);
iDay := DAY_OF_YEAR(dtToday);

IF iDAY <= 15 OR iDAY > 330 THEN
	F_IS_ADVENT := TRUE;
ELSE
	F_IS_ADVENT := FALSE;
END_IF

FUNCTION F_IS_NIGHT : BOOL
VAR_INPUT
	dtCurrent : DATE_AND_TIME;
END_VAR
VAR
	dtToday : DATE;
	iMonth : INT;
	iHour : INT;
	tagTabelle : ARRAY[1..12, 1..2] OF BYTE :=
		8, 17,
		8, 17,
		7, 18,
		7, 19,
		6, 20,
		5, 21,
		5, 21,
		6, 20,
		7, 19,
		7, 18,
		8, 17,
		8, 17;
END_VAR


(* einfachste Berechnung �ber eine Tabelle *)

dtToday := DT_TO_DATE(dtCurrent);
iMonth := MONTH_OF_DATE(dtToday);
iHour := HOUR_OF_DT(dtCurrent);

IF iHour <= tagTabelle[iMonth,1] OR iHour >= tagTabelle[iMonth,2] THEN
	F_IS_NIGHT := TRUE;
ELSE
	F_IS_NIGHT := FALSE;
END_IF


PROGRAM MAIN
VAR
	(* --- WZ: 2 Deckenleuchten --- *)

	fbSWITCH_P : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
	fbSWITCH_S : SWITCH_I;

(* --- WZ: Rolladensteuerung, Fenster --- *)

    	fbBLIND_INPUT_WINDOW : BLIND_INPUT;
	fbBLIND_NIGHT_WINDOW : BLIND_NIGHT;
    	fbBLIND_X_WINDOW : BLIND_CONTROL_S;

	stBlindStatusWindow AT %MB0 : ST_BlindStatus;

(* --- WZ: Rolladensteuerung, Tuer --- *)

    	fbBLIND_INPUT_DOOR : BLIND_INPUT;
(*	fbBLIND_NIGHT_DOOR : BLIND_NIGHT; *)
    	fbBLIND_X_DOOR : BLIND_CONTROL_S;

	stBlindStatusDoor AT %MB2 : ST_BlindStatus;

(* --- Terasse: 2 Taster und 2 Lampen --- *)

 	fbSWITCH_T1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_T2 : SWITCH_I;

	(* --- 3 Taster und 2 Lampen am Kellerabgang --- *)

	fbDUAL_LAMP : FB_DUAL_LAMP; (* from my own function blocks *)

(* --- Steckdosen --- *)

	fbSteckdose1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
	fbSteckdose2 : SWITCH_I;
	fbSteckdose3 : SWITCH_I;
	fbSteckdose4 : SWITCH_I;
	fbSteckdose5 : SWITCH_I;
	fbSteckdose6 : SWITCH_I;
	fbSteckdose7 : SWITCH_I;
	fbSteckdose8 : SWITCH_I;
	fbSteckdose9 : SWITCH_I;
	fbSteckdose10 : SWITCH_I;
	fbSteckdoseGarten : SWITCH_I;

(* - Funktionsblock RTC und interface zum Stellen der RTC - *)

	stSPSmailbox AT %MB4 : ST_SPSMailbox; (* 2 Bytes Groesse *)
	stRTCData AT %MB6 : ST_RTCData; (* 8 Bytes, direkt im Anschluss an stSPSMailbox *)

	fbRTC : RTC;	(* - ben�tigt: Standard.lb6, TcPlcUtilitiesBC.lb6, PlcSystemBC.lb6 - *)

	stSPSinCopy AT %MB16 : ST_SPSinVariables;
	stSPSoutCopy AT %MB20 : ST_SPSoutVariables;

	bNacht : BOOL := FALSE; (* ist nachts TRUE *)
	bAdvent : BOOL; (* ist im Advent TRUE, fuer Adventsbeleuchtung *)

	fbTimer : TIMER_1;  (* OSCAT building lib 100, function block TIMER_1 *)

	bFirst : BOOL := TRUE;    (* 1. Zyklus der SPS *)
END_VAR

(* - einmalig, zu Programmbeginn - *)

IF bFirst THEN
	bFirst := FALSE;

	(* Echtzeituhr: stellen = preset *)
	fbRTC(EN := TRUE, PDT := DT#2014-05-08-18:30:00);
END_IF

(* - Input-Variablen in den Merker-Bereich kopieren - *)

(*
MEMCPY(ADR(stSPSinCopy), ADR(stSPSinVar),SIZEOF(ST_SPSinVariables));
*)

(* - RTC ggf. stellen (FB) und Zeit weiterzaehlen - *)

SET_RTC(ADR(stSPSmailbox), ADR(stRTCData), ADR(fbRTC));

fbRTC(EN := FALSE);   (* muss einmal pro PLC-Zyklus gerufen werden *)

(* - feststellen, ob es Nacht ist - *)

bNacht := F_IS_NIGHT(fbRTC.CDT);

(* - WZ: Schema f�r 'Hauptbeleuchtung (ex. Couchtisch)' - *)

fbSWITCH_P(IN := stSPSinVar.bTASTER_P1 OR stSPSinVar.bTASTER_S1, T_DEBOUNCE := t#20ms );

stSPSoutVar.bLampe1 := fbSWITCH_P.Q;

(* - WZ: Schema f�r  'Nebenbeleuchtung (ex. Esstisch)' - *)

fbSWITCH_S(IN := stSPSinVar.bTASTER_P2 OR stSPSinVar.bTASTER_S2, T_DEBOUNCE := t#20ms );

stSPSoutVar.bLampe2 := fbSWITCH_S.Q;

(* - Schema fuer Rolladen, Fenster - *)

fbBLIND_INPUT_WINDOW(S1 := stSPSinVar.bTASTER_BLIND_WINDOW_UP,
		S2 := stSPSinVar.bTASTER_BLIND_WINDOW_DOWN,
                IN := FALSE,
(* TRUE fuer Single-Click Mode *)
                CLICK_EN := TRUE,
(* Timeout fuer Click-Erkennung *)
		CLICK_TIME := t#300ms,
                pos := fbBLIND_X_WINDOW.POS,
(* Position wenn IN == TRUE *)
                pi := 16#FF,
(* Timeout fuer Handbetrieb *)
                MANUAL_TIMEOUT := t#2m,
(* Timeout fuer eine Bewegung *)
                MAX_RUNTIME := t#20s,
(* zur�ck zur alten Position wenn Zwang weg *)
                MASTER_MODE := TRUE);

fbBLIND_NIGHT_WINDOW(UP := fbBLIND_INPUT_WINDOW.QU,
		DN := fbBLIND_INPUT_WINDOW.QD,
		S_IN := fbBLIND_INPUT_WINDOW.STATUS,
		pi := fbBLIND_INPUT_WINDOW.PO,
(* Tagschaltung aktiv/inaktive *)
		E_DAY := TRUE,
(* Nachtschaltung aktiv/inaktiv *)
		E_NIGHT := TRUE,
(* aktuelle Zeit/Datum *)
		DTIN := fbRTC.CDT,
(* Sonnenaufgangszeit *)
		SUNRISE := tod#06:00,
(* Offset von Sonnenaufgang [Minuten] *)
		SUNRISE_OFFSET := t#0m,
(* Sonnenuntergangszeit *)
		SUNSET := tod#21:00,
(* Offset vom Sonnenuntergang [Minuten] *)
		SUNSET_OFFSET := t#0m,
(* Position Nacht *)
		NIGHT_POSITION := 16#60);

fbBLIND_X_WINDOW(UP := fbBLIND_NIGHT_WINDOW.QU,
                DN := fbBLIND_NIGHT_WINDOW.QD,
                S_IN := fbBLIND_INPUT_WINDOW.STATUS,
                pi := fbBLIND_INPUT_WINDOW.PO,
                T_LOCKOUT := t#200ms,
(* Verlaengerungszeit bei Endanschlag, benoetigt EXT_TRG zur Aktivierung *)
                T_EXT := t#2s,
(* Laufzeit des Rollos, Richtung  'UP' *)
                T_UP := t#15s,
(* Laufzeit des Rollos, Richtung 'DOWN' *)
                T_DN := t#15s);

stSPSoutVar.bMOTOR_BLIND_WINDOW_UP := fbBLIND_X_WINDOW.MU;
stSPSoutVar.bMOTOR_BLIND_WINDOW_DOWN := fbBLIND_X_WINDOW.MD;

stBlindStatusWindow.Position := fbBLIND_X_WINDOW.POS;
stBlindStatusWindow.Status := fbBLIND_X_WINDOW.STATUS;

(* - Schema fuer Rolladen, Tuer - *)

fbBLIND_INPUT_DOOR(S1 := stSPSinVar.bTASTER_BLIND_DOOR_UP,
		S2 := stSPSinVar.bTASTER_BLIND_DOOR_DOWN,
                IN := FALSE,
(* TRUE fuer Single-Click Mode *)
                CLICK_EN := TRUE,
(* Timeout fuer Click-Erkennung *)
		CLICK_TIME := t#300ms,
                pos := fbBLIND_X_DOOR.POS,
(* Position wenn IN == TRUE *)
                pi := 16#FF,
(* Timeout fuer Handbetrieb *)
                MANUAL_TIMEOUT := t#2m,
(* Timeout fuer eine Bewegung *)
                MAX_RUNTIME := t#24s);

fbBLIND_X_DOOR(UP := fbBLIND_INPUT_DOOR.QU,
                DN := fbBLIND_INPUT_DOOR.QD,
                S_IN := fbBLIND_INPUT_DOOR.STATUS,
                pi := fbBLIND_INPUT_DOOR.PO,
                T_LOCKOUT := t#200ms,
(* Verlaengerungszeit bei Endanschlag, benoetigt EXT_TRG zur Aktivierung *)
                T_EXT := t#2s,
(* Laufzeit des Rollos, Richtung  'UP' *)
                T_UP := t#22s,
(* Laufzeit des Rollos, Richtung 'DOWN' *)
                T_DN := t#22s);

stSPSoutVar.bMOTOR_BLIND_DOOR_UP := fbBLIND_X_DOOR.MU;
stSPSoutVar.bMOTOR_BLIND_DOOR_DOWN := fbBLIND_X_DOOR.MD;

stBlindStatusDoor.Position := fbBLIND_X_DOOR.POS;
stBlindStatusDoor.Status := fbBLIND_X_DOOR.STATUS;

(* - WZ: Schema fuer 10 Steckdosen, bSteckdose5 nicht beschaltet - *)

fbSteckdose1(IN := stSPSinVar.bTASTER_P4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose1 := fbSteckdose1.Q; (* an Eingangstuer *)
stSPSoutVar.bSteckdose2 := fbSteckdose1.Q; (* an Durchgangstuer *)

(*
fbSteckdose2(IN := stSPSinVar.bTASTER_S3, T_DEBOUNCE := t#20ms );
bSteckdose2 := fbSteckdose2.Q; (* an Durchgangstuer *)
*)

fbSteckdose3(IN := stSPSinVar.bTASTER_S4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose3 := fbSteckdose3.Q; (* in Ecke bei Durchgangstuer, fuer Schrankbeleuchtung *)

fbSteckdose4(IN := stSPSinVar.bTASTER_G3, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose4 := fbSteckdose4.Q; (* in Ecke bei Terassentuer *)

fbSteckdose5(IN := FALSE, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose5 := fbSteckdose5.Q;

fbSteckdose6(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose6 := fbSteckdose6.Q;  (* in Ecke bei Terassentuer *)

(* Setze bAdvent im Dezember und bis zum 15. Januar (Funktion F_IS_ADVENT() *)

bAdvent := F_IS_ADVENT(fbRTC.CDT);

(* Anschalten mit Timer, benutzt Startzeit und Intervall.
   Alternative: Funktion TIMECHECK() *)
fbTIMER(E := bAdvent, DTI := fbRTC.CDT, START := tod#17:00:00, DURATION := t#7h00m, DAY := 2#0111_1111);
fbSteckdose7(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms );
IF bADVENT THEN
	stSPSoutVar.bSteckdose7 := fbTIMER.Q; (* Ecke, fuer Adventsbeleuchtung *)
ELSE
	stSPSoutVar.bSteckdose7 := fbSteckdose7.Q;
END_IF

fbSteckdose8(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms ); (* in Ecke, fuer ... *)
stSPSoutVar.bSteckdose8 := fbSteckdose8.Q;

fbSteckdose9(IN := stSPSinVar.bTASTER_P3, T_DEBOUNCE := t#20ms ); (* Multimediawand, linke Steckdose *)
stSPSoutVar.bSteckdose9 := fbSteckdose9.Q;

fbSteckdose10(IN := stSPSinVar.bTASTER_P3, T_DEBOUNCE := t#20ms ); (* Multimediawand, rechte Steckdose *)
stSPSoutVar.bSteckdose10 := fbSteckdose10.Q;

(* - Schemata fuer die Terasse (z.Zt. mit EG2/S3 verbunden) - *)

IF NOT bNacht THEN
	(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_EG2, T_on_max := t#60s, T_DEBOUNCE := t#20ms); *)
	(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_T1, T_on_max := t#60s, T_DEBOUNCE := t#20ms); *)
	fbSWITCH_T1(IN := stSPSinVar.bTASTER_S3 OR stSPSinVar.bTASTER_T1, T_on_max := t#60s, T_DEBOUNCE := t#20ms);
ELSE
	(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_EG2, T_on_mxx:t#0s, T_DEBOUNCE := t#20ms); *)
	(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_T1, T_on_mxx:t#0s, T_DEBOUNCE := t#20ms); *)
	fbSWITCH_T1(IN := stSPSinVar.bTASTER_S3 OR stSPSinVar.bTASTER_T1, T_on_max := t#0s, T_DEBOUNCE := t#20ms);
END_IF
stSPSoutVar.bLampe_T1 := fbSWITCH_T1.Q;

(* fuer beide Lampen evtl. Serienschaltung nehmen ... *)
fbSWITCH_T2(IN := stSPSinVar.bTASTER_T2, T_DEBOUNCE := t#20ms);

stSPSoutVar.bLampe_T2 := fbSWITCH_T2.Q;

(* Steckdose auf der Terasse *)

fbSteckdoseGarten(IN := FALSE, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose11 := fbSteckdoseGarten.Q; (* Steckdose auf Terasse *)

(* - Schema f�r 'Kellerabgang', keine Debug-Ausgabe - *)

fbDUAL_LAMP(in1 := stSPSinVar.bTASTER_EG1,
	in2 := stSPSinVar.bTASTER_KG,
	in3 := FALSE,
        use_timer := TRUE,
	T_double_wait := t#3000ms );

stSPSoutVar.bLampe_EG := fbDUAL_LAMP.out1;
stSPSoutVar.bLampe_KG := fbDUAL_LAMP.out2;

(* - Output-Variablen in den Merker-Bereich kopieren - *)

(*
MEMCPY(ADR(stSPSoutCopy),ADR(stSPSoutVar),SIZEOF(ST_SPSoutVariables));
*)
