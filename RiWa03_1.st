(*
   Project: RiWa03_1 - 20130803
   
   Author:  HJM
   
   Features: - Steuerung fuer Kellerabgang (2 Taster & 2 Lampen)
             - Wohnzimmerlicht (2*2 Taster & 2 Lampen)
   
   $Id: RiWa03_1.st 17610 2014-01-18 08:27:23Z mathes $
 *)


PROGRAM MAIN
VAR
	(* --- WZ: 4 Taster bei Eingangst�r (P)  --- *)

	bTASTER_P1 AT %I* : BOOL;
	bTASTER_P2 AT %I* : BOOL;
	bTASTER_P3 AT %I* : BOOL;
	bTASTER_P4 AT %I* : BOOL;

	(* --- WZ: 4 Taster bei Durchgangst�r (S)  --- *)

	bTASTER_S1 AT %I* : BOOL;
	bTASTER_S2 AT %I* : BOOL;
	bTASTER_S3 AT %I* : BOOL;
	bTASTER_S4 AT %I* : BOOL;

	(* --- 2 Deckenleuchten --- *)

	bLAMP1 AT %Q* : BOOL;
	bLAMP2 AT %Q* : BOOL;

	fbSWITCH_P : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
	fbSWITCH_S : SWITCH_I;

	(* --- 2 Taster und 2 Lampen am Kellerabgang --- *)

	bTASTER_EG AT %I* : BOOL;
	bTASTER_KG AT %I* : BOOL;
	bLAMPE_EG AT %Q* : BOOL;
	bLAMPE_KG AT %Q* : BOOL;

	fbDUAL_LAMP : FB_DUAL_LAMP; (* from my own function blocks *)

	(* --- Echtzeituhr, ben�tigte Libs:  Standard.lb6, TcPlcUtilitiesBC.lb6, PlcSystemBC.lb6 --- *)

	fbRTC : RTC;

	bFirst : BOOL := TRUE;
END_VAR
VAR_CONFIG
	(* --- KL1104 an Position 1, 2 Koppelrelais --- *)
	main.bTASTER_EG AT %IX0.0 : BOOL;
	main.bTASTER_KG AT %IX0.1 : BOOL;

	(* --- KL1104 an Position 2, 2 Koppelrelais --- *)
	main.bTASTER_P1 AT %IX0.4 : BOOL;
	main.bTASTER_P2 AT %IX0.5 : BOOL;
	main.bTASTER_P3 AT %IX0.6 : BOOL;
	main.bTASTER_P4 AT %IX0.7 : BOOL;

	(* --- KL1104 an Position 3 --- *)
	main.bTASTER_S1 AT %IX1.0 : BOOL;
	main.bTASTER_S2 AT %IX1.1 : BOOL;
	main.bTASTER_S3 AT %IX1.2 : BOOL;
	main.bTASTER_S4 AT %IX1.3 : BOOL;

	(* --- KL9150 ... / 750-611 an Position 7 --- *)

	(* --- KL2622 / 750-512 an Position 8 --- *)
	main.bLAMPE_EG AT %QX0.0 : BOOL;
	main.bLAMPE_KG AT %QX0.1 : BOOL;

	(* --- KL2622 / 750-512 an Position 9 --- *)
	main.bLAMP1 AT %QX0.2 : BOOL;
	main.bLAMP2 AT %QX0.3 : BOOL;

END_VAR

(* --- einmalig, zu Programmbeginn --- *)

IF bFirst THEN
	bFirst := FALSE;

	(* Echtzeituhr: stellen = preset *)
	fbRTC(EN := TRUE, PDT := DT#2013-07-27-08:00:00);
END_IF

(* --- Zeit von RTC holen ... --- *)

fbRTC(EN := FALSE);

(* --- WZ: Schema f�r 'Hauptbeleuchtung (ex. Couchtisch)' --- *)

fbSWITCH_P(IN := bTASTER_P1 OR bTASTER_S1 (* OR bTASTER_G2 *), T_DEBOUNCE := t#20ms );

bLAMP1 := fbSWITCH_P.Q;

(* --- WZ: Schema f�r  'Nebenbeleuchtung (ex. Esstisch)' --- *)

fbSWITCH_S(IN := bTASTER_P2 OR bTASTER_S2, T_DEBOUNCE := t#20ms );

bLAMP2 := fbSWITCH_S.Q;

(* --- Schema f�r 'Kellerabgang', keine Debug-Ausgabe --- *)

fbDUAL_LAMP(in1 := bTASTER_EG, in2 := bTASTER_KG,
             use_timer := TRUE, T_double_wait := t#3000ms );

bLAMPE_EG := fbDUAL_LAMP.out1;
bLAMPE_KG := fbDUAL_LAMP.out2;
