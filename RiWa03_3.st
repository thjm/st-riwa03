(*
   Project: RiWa03_3 - 20131221
   
   Author:  HJM
   
   Features: - Steuerung fuer Kellerabgang (2 Taster & 2 Lampen)
             - Wohnzimmerlicht (2*2 Taster & 2 Lampen)
             - Diagnostik fuer Einspeiseklemme 24V
             - 2 Lampen auf Terasse
             - Steckdosen ueber Schalter
             - Steckdose mit Zeituhr
             - Rolladen (T�r) �ber Taster
   
   $Id: RiWa03_3.st 17927 2014-02-19 19:14:22Z mathes $
 *)
 
 
PROGRAM MAIN
VAR

(* - WZ: 4 Taster bei Eingangst�r (P) -- *)

	bTASTER_P1 AT %I* : BOOL;
	bTASTER_P2 AT %I* : BOOL;
	bTASTER_P3 AT %I* : BOOL;
	bTASTER_P4 AT %I* : BOOL;

(* - WZ: 3 Taster bei Durchgangst�r (S)  - *)

	bTASTER_S1 AT %I* : BOOL;
	bTASTER_S2 AT %I* : BOOL;
	bTASTER_S3 AT %I* : BOOL;
	bTASTER_S4 AT %I* : BOOL;

(* - WZ: 2 Deckenleuchten - *)

 	bLAMP1 AT %Q* : BOOL;
 	bLAMP2 AT %Q* : BOOL;

 	fbSWITCH_P : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_S : SWITCH_I;

(* - WZ: 4 Taster an Gartentuer - *)
^
	bTASTER_G1 AT %I* : BOOL;
	bTASTER_G2 AT %I* : BOOL;
	bTASTER_G3 AT %I* : BOOL;
	bTASTER_G4 AT %I* : BOOL;

(* - WZ: Rolladensteuerung, Fenster - *)

	bTASTER_BLIND_WINDOW_DOWN AT %I* : BOOL;
	bTASTER_BLIND_WINDOW_UP AT %I* : BOOL;

	bACT_BLIND_WINDOW_DOWN AT %Q* : BOOL;
	bACT_BLIND_WINDOW_UP AT %Q* : BOOL;

	fbBLIND_INPUT_WINDOW : BLIND_INPUT;
	fbBLIND_X_WINDOW : BLIND_CONTROL_S;

(* - WZ: Rolladensteuerung, Tuer - *)

	bTASTER_BLIND_DOOR_DOWN AT %I* : BOOL;
	bTASTER_BLIND_DOOR_UP AT %I* : BOOL;

	bACT_BLIND_DOOR_DOWN AT %Q* : BOOL;
	bACT_BLIND_DOOR_UP AT %Q* : BOOL;

	fbBLIND_INPUT_DOOR : BLIND_INPUT;
	fbBLIND_X_DOOR : BLIND_CONTROL_S;

(* - Terasse: 2 Taster und 2 Lampen - *)

	bTASTER_T1 AT %I* : BOOL;
	bTASTER_T2 AT %I* : BOOL;

	bLAMPE_T1 AT %Q* : BOOL;
	bLAMPE_T2 AT %Q* : BOOL;

 	fbSWITCH_T1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_T2 : SWITCH_I;

(* - Keller: 2 Taster und 2 Lampen am Kellerabgang - *)

	bTASTER_EG1 AT %I* : BOOL;
	bTASTER_EG2 AT %I* : BOOL;
 	bTASTER_KG AT %I* : BOOL;
 	bLAMPE_EG AT %Q* : BOOL;
 	bLAMPE_KG AT %Q* : BOOL;
 
 	fbDUAL_LAMP : FB_DUAL_LAMP; (* from my own function blocks *)

(* - KL2134, fuer Steckdosen, via Relais - *)

	bSteckdose1 AT %Q* : BOOL;
	bSteckdose2 AT %Q* : BOOL;
	bSteckdose3 AT %Q* : BOOL;
	bSteckdose4 AT %Q* : BOOL;

(* - KL2134, fuer Steckdosen, via Relais - *)

	bSteckdose9 AT %Q* : BOOL;
	bSteckdose10 AT %Q* : BOOL;
	bSteckdose7 AT %Q* : BOOL;
	bSteckdose8 AT %Q* : BOOL;

(* - KL2134, fuer Steckdosen, via Relais (fehlt noch)  - *)

	bSteckdose5 AT %Q* : BOOL; (* nicht benutzt *
	bSteckdose6 AT %Q* : BOOL;
	bSteckdose11 AT %Q* : BOOL;

(* - Funktionsbausteine zum Schalter der Steckdosen - *)

	fbSOCKET1 : SWITCH_I;
	fbSOCKET2 : SWITCH_I;
	fbSOCKET3 : SWITCH_I;
	fbSOCKET4 : SWITCH_I;
	fbSOCKET5 : SWITCH_I;
	fbSOCKET6 : SWITCH_I;
	fbSOCKET7 : SWITCH_I;
	fbSOCKET8 : SWITCH_I;
	fbSOCKET9 : SWITCH_I;
	fbSOCKET10 : SWITCH_I;
	fbSOCKET11 : SWITCH_I;

(* - Diagnostic fuer Einspeiseklemme - *)

	bPowerGood AT %I* : BOOL;
	bFuseFail AT %I* : BOOL;

(* - Echtzeituhr, ben�tigte Libs:  Standard.lb6, TcPlcUtilitiesBC.lb6, PlcSystemBC.lb6 - *)

 	fbRTC : RTC;

	bADVENT : BOOL := TRUE;              (*Adventsbeleuchtung aktiv ... *)
	fbTimer : TIMER_1;
 	bFirst : BOOL := TRUE;
END_VAR

VAR_CONFIG

(* - KL1104 an Position 1, 2 x Koppelrelais - *)

	main.bTASTER_EG1 AT %IX0.0 : BOOL;
	main.bTASTER_KG AT %IX0.1 : BOOL;
	main.bTASTER_EG2 AT %IX0.2 : BOOL;
	(* main.DUMMY at %IX0.3 . bool; *)

(* - KL1104 an Position 2, 4 x Koppelrelais - *)

	main.bTASTER_P1 AT %IX0.4 : BOOL;
	main.bTASTER_P2 AT %IX0.5 : BOOL;
	main.bTASTER_P3 AT %IX0.6 : BOOL;
	main.bTASTER_P4 AT %IX0.7 : BOOL;

(* - KL1104 an Position 3, 2 x Koppelrelais - *)

	main.bTASTER_S1 AT %IX1.0 : BOOL;
	main.bTASTER_S2 AT %IX1.1 : BOOL;
	main.bTASTER_S3 AT %IX1.2 : BOOL;
	main.bTASTER_S4 AT %IX1.3 : BOOL;

(* - KL1104 an Position 4 - *)

	main.bTASTER_BLIND_WINDOW_UP AT %IX1.4 : BOOL;
	main.bTASTER_BLIND_WINDOW_DOWN AT %IX1.5 : BOOL;
	main.bTASTER_BLIND_DOOR_UP AT %IX1.6 : BOOL;
	main.bTASTER_BLIND_DOOR_DOWN AT %IX1.7 : BOOL;

(* - KL1104 an Position 5 - *)

	main.bTASTER_G1 AT %IX2.0 : BOOL;
	main.bTASTER_G2 AT %IX2.1 : BOOL;
	main.bTASTER_G3 AT %IX2.2 : BOOL;
	main.bTASTER_G4 AT %IX2.3 : BOOL;

(* - KL1104 an Position 6-- *)

	main.bTASTER_T1 AT %IX2.4 : BOOL;
	main.bTASTER_T2 AT %IX2.5 : BOOL;
	(* main.bSPARE_IN AT %IX2.6 : BOOL; *)
	(* main.bSPARE_IN AT %IX2.7 : BOOL; *)

(* - 750-609 (mit Diagnostik) an Position 7 - *)

	main.bPowerGood AT %IX3.0 : BOOL;
	main.bFuseFail AT %IX3.1 : BOOL;

(* - KL2134 an Position 8 - *)

	main.bSteckdose1 AT %QX0.0 : BOOL;
	main.bSteckdose2 AT %QX0.1 : BOOL;
	main.bSteckdose3 AT %QX0.2 : BOOL;
	main.bSteckdose4 AT %QX0.3 : BOOL;

(* - KL2134 an Position 9 - *)

	main.bSteckdose9 AT %QX0.4 : BOOL;
	main.bSteckdose10 AT %QX0.5 : BOOL;
	main.bSteckdose7 AT %QX0.6 : BOOL;
	main.bSteckdose8 AT %QX0.7 : BOOL;

(* - KL2134 an Position 10 - *)

	main.bSteckdose5 AT %QX2.4 : BOOL;
	main.bSteckdose6 AT %QX2.5 : BOOL;
	main.bSteckdose11 AT %QX2.6 : BOOL;
(*	main.bSPARE_OUT AT %QX2.7 : BOOL; *)

(* - KL9180, Trennklemme (fehlt noch) - *)

(* - KL9150 ... / 750-611 an Positon 11, Einspeisung Keller - *)

(* - KL2622 / 750-512 an Position 12 - *)

	main.bLAMPE_EG AT %QX1.0 : BOOL;
	main.bLAMPE_KG AT %QX1.1 : BOOL;

(* - KL9150 ... / 750-611 an Position 13, Einspeisung Wohnzimmer/Garten - *)

(* - KL2622 / 750-512 an Position 14 - *)

	main.bLAMP1 AT %QX1.2 : BOOL;
	main.bLAMP2 AT %QX1.3 : BOOL;

(* - KL2622 / 750-512 an Position 15 - *)

	main.bLAMP_T1 AT %QX1.4 : BOOL;
	main.bLAMP_T2 AT %QX1.5 : BOOL;

(* - KL2622 / 750-512 an Position 16 - *)

	main.bMOTOR_BLIND_WINDOW_UP AT %QX1.6 : BOOL;
	main.bMOTOR_BLIND_WINDOW_DOWN AT %QX1.7 : BOOL;

(* - KL2622 / 750-512 an Position 17 - *)

	main.bMOTOR_BLIND_DOOR_UP AT %QX2.0 : BOOL;
	main.bMOTOR_BLIND_DOOR_DOWN AT %QX2.1 : BOOL;
END_VAR


(* - einmalig, zu Programmbeginn - *)

IF bFirst THEN
	bFirst := FALSE;

	(* Echtzeituhr: stellen = preset *)
	fbRTC(EN := TRUE, PDT := DT#2013-12-21-12:00:00);
END_IF

(* - Zeit von RTC holen ... - *)

fbRTC(EN := FALSE);

(* - WZ: Schema f�r 'Hauptbeleuchtung (ex. Couchtisch)' - *)

fbSWITCH_P(IN := bTASTER_P1 OR bTASTER_S1, T_DEBOUNCE := t#20ms );

bLAMP1 := fbSWITCH_P.Q;

(* - WZ: Schema f�r  'Nebenbeleuchtung (ex. Esstisch)' - *)

fbSWITCH_S(IN := bTASTER_P2 OR bTASTER_S2, T_DEBOUNCE := t#20ms );

bLAMP2 := fbSWITCH_S.Q;

(* - Schema fuer Rolladen, Fenster - *)

(* - Schema fuer Rolladen, Tuer - *)

fbBLIND_INPUT_DOOR(S1 := bTASTER_BLIND_DOOR_UP,
	      S2 := bTASTER_BLIND_DOOR_DOWN,
              IN := FALSE,
(* TRUE fuer Single-Click Mode *)
              CLICK_EN := TRUE,
(* Timeout fuer Click-Erkennung *)
			  CLICK_TIME := t#300ms,
              pos := fbBLIND_X_DOOR.POS,
(* Position wenn IN == TRUE *)
              pi := 16#FF,
(* Timeout fuer Handbetrieb *)
              MANUAL_TIMEOUT := t#2m,
(* Timeout fuer eine Bewegung *)
              MAX_RUNTIME := t#24s);

fbBLIND_X_DOOR(UP := fbBLIND_INPUT_DOOR.QU,
              DN := fbBLIND_INPUT_DOOR.QD,
              S_IN := fbBLIND_INPUT_DOOR.STATUS,
              pi := fbBLIND_INPUT_DOOR.PO,
              T_LOCKOUT := t#200ms,
(* Verlaengerungszeit bei Endanschlag, benoetigt EXT_TRG zur Aktivierung *)
              T_EXT := t#2s,
(* Laufzeit des Rollos, Richtung  'UP' *)
              T_UP := t#22s,
(* Laufzeit des Rollos, Richtung 'DOWN' *)
              T_DN := t#22s);

bMOTOR_BLIND_DOOR_UP := fbBLIND_X_DOOR.MU;
bMOTOR_BLIND_DOOR_DOWN := fbBLIND_X_DOOR.MD;

(* - WZ: Schema fuer 10 Steckdosen, bSteckdose5 nicht beschaltet - *)

fbSOCKET1(IN := bTASTER_P4, T_DEBOUNCE := t#20ms );
bSteckdose1 := fbSOCKET1.Q; (* an Eingangstuer *)
bSteckdose2 := fbSOCKET1.Q; (* an Durchgangstuer *)

(*
fbSOCKET2(IN := bTASTER_S3, T_DEBOUNCE := t#20ms );
bSteckdose2 := fbSOCKET2.Q; (* an Durchgangstuer *)
*)

fbSOCKET3(IN := bTASTER_S4, T_DEBOUNCE := t#20ms );
bSteckdose3 := fbSOCKET3.Q; (* in Ecke bei Durchgangstuer, fuer Schrankbeleuchtung *)

fbSOCKET4(IN := bTASTER_G3, T_DEBOUNCE := t#20ms );
bSteckdose4 := fbSOCKET4.Q; (* in Ecke bei Terassentuer *)

fbSOCKET5(IN := FALSE, T_DEBOUNCE := t#20ms );
bSteckdose5 := fbSOCKET5.Q;

fbSOCKET6(IN := bTASTER_G4, T_DEBOUNCE := t#20ms );
bSteckdose6 := fbSOCKET6.Q;  (* in Ecke bei Terassentuer *)

fbTIMER(E := bADVENT, DTI := fbRTC.CDT, START := tod#17:30:00, DURATION := t#6h30m, DAY := 2#0111_1111);
fbSOCKET7(IN := FALSE, T_DEBOUNCE := t#20ms );
IF bADVENT THEN
	bSteckdose7 := fbTIMER.Q; (* Ecke, fuer Adventsbeleuchtung *)
ELSE
	bSteckdose7 := fbSOCKET7.Q;
END_IF

fbSOCKET8(IN := bTASTER_G4, T_DEBOUNCE := t#20ms ); (* in Ecke, fuer ... *)
bSteckdose8 := fbSOCKET8.Q;

fbSOCKET9(IN := bTASTER_P3, T_DEBOUNCE := t#20ms ); (* Multimediawand, linke Steckdose *)
bSteckdose9 := fbSOCKET9.Q;

fbSOCKET10(IN := bTASTER_P3, T_DEBOUNCE := t#20ms ); (* Multimediawand, rechte Steckdose *)
bSteckdose10 := fbSOCKET10.Q;

(* - Schemata fuer die Terasse (z.Zt. mit EG2 verbunden) - *)

(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_EG2, T_DEBOUNCE := t#20ms); *)
fbSWITCH_T1(IN := bTASTER_S3, T_DEBOUNCE := t#20ms);
(* fbSWITCH_T1(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms); *)
bLampe_T1 := fbSWITCH_T1.Q;

(* fuer beide Lampen evtl. Serienschaltung nehmen ... *)
bLAMPE_T2 := FALSE;

fbSOCKET11(IN := FALSE, T_DEBOUNCE := t#20ms );
bSteckdose11 := fbSOCKET11.Q; (* Steckdose auf Terasse *)

(* - Schema f�r 'Kellerabgang', keine Debug-Ausgabe - *)

fbDUAL_LAMP(in1 := bTASTER_EG1,
		in2 := bTASTER_KG,
		in3 := bTASTER_EG2,
        use_timer := TRUE,
		T_double_wait := t#3000ms );

bLAMPE_EG := fbDUAL_LAMP.out1;
bLAMPE_KG := fbDUAL_LAMP.out2;

