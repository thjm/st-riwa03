(*

   Two functions to get the time (type TOD) of sunrise and sunset. They are based on
   beforehand calculated tables for the specific location.

   $Id: GetSun.st 19438 2014-10-25 12:14:04Z mathes $
 
 *)
 
FUNCTION F_GET_SUNRISE : TOD
VAR_INPUT
	dtCurrent : DATE_AND_TIME;
END_VAR
VAR
	dtToday : DATE;
	iDay : INT;
END_VAR

dtToday := DT_TO_DATE(dtCurrent);
iDay := DAY_OF_YEAR(dtToday);

(* Table contains minutes since midnight which must be converted into TOD type. *)
F_GET_SUNRISE := HOUR_TO_TOD(INT_TO_REAL(sunRiseConst[iDay]) / 60.0);


FUNCTION F_GET_SUNSET : TOD
VAR_INPUT
	dtCurrent : DATE_AND_TIME;
END_VAR
VAR
	dtToday : DATE;
	iDay : INT;
END_VAR

dtToday := DT_TO_DATE(dtCurrent);
iDay := DAY_OF_YEAR(dtToday);

(* Table contains minutes since midnight which must be converted into TOD type. *)
F_GET_SUNSET := HOUR_TO_TOD(INT_TO_REAL(sunSetConst[iDay]) / 60.0);
