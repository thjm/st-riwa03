(* $Id: ST_SPSoutVariables.st 20872 2015-03-31 18:38:49Z mathes $ *)

TYPE ST_SPSoutVariables :
STRUCT
	(* - KL1408 an Position 2, Reserve - *)
	(* - KL1104 an Position 3, 2 Koppelrelais - *)
	(* - KL1104 an Position 4 - *)
	(* - KL1104 an Position 5 - *)
	(* - KL1104 an Position 6 - *)
(* - 750-609 (mit Diagnostik) an Position 7 - *)

(* - KL2134 an Position 8 - *)

	bSteckdose1 AT %QX0.0 : BOOL;
	bSteckdose2 AT %QX0.1 : BOOL;
	bSteckdose3 AT %QX0.2 : BOOL;
	bSteckdose4 AT %QX0.3 : BOOL;

(* - KL2134 an Position 9 - *)

	bSteckdose9 AT %QX0.4 : BOOL;
	bSteckdose10 AT %QX0.5 : BOOL;
	bSteckdose7 AT %QX0.6 : BOOL;
	bSteckdose8 AT %QX0.7 : BOOL;

(* - KL2134 an Position 10 - *)

	bSteckdose5 AT %QX1.0 : BOOL;
	bSteckdose6 AT %QX1.1 : BOOL;
	bSteckdose11 AT %QX1.2 : BOOL;
	bOUT_1_3 AT %QX1.3 : BOOL;

(* - Tbd: KL6180 (Trennklemme) an Position ?? - *)

(* - KL9150 ... / 750-611 an Position 11 - *)
(* - KL1702 / 750-405 an Position 12 - *)

(* - KL2602 / 750-512 an Position 13 - *)
	bLampe_EG AT %QX1.4 : BOOL;
	bLampe_KG AT %QX1.5 : BOOL;

(* - KL9020 (K-Bus-Verlängerung) an Position 14 - *)
(* - KL9050 (K-Bus ??) an Position 15 - *)

	(* - KL9150 ... / 750-611 an Position 16 - *)

	(* - KL2602 / 750-512 an Position 17 - *)
	bLampe1 AT %QX1.6 : BOOL;
	bLampe2 AT %QX1.7 : BOOL;

	(* - KL2602 / 750-512 an Position 18 - *)
	bLampe_T1 AT %QX2.0 : BOOL;
	bLampe_T2 AT %QX2.1 : BOOL;

(* - KL1702 / 750-405 an Position 19 - *)

	(* - KL2602 / KL2622 / 750-512 an Position 20 - *)
	bMOTOR_BLIND_WINDOW_UP AT %QX2.2 : BOOL;
	bMOTOR_BLIND_WINDOW_DOWN AT %QX2.3 : BOOL;

(* - KL1702 / 750-405 an Position 21 - *)

	(* - KL2602 / KL2622 / 750-512 an Position 22 - *)
	bMOTOR_BLIND_DOOR_UP AT %QX2.4 : BOOL;
	bMOTOR_BLIND_DOOR_DOWN AT %QX2.5 : BOOL;

(* - KL9010 (Endklemme) an Position 23 - *)

END_STRUCT
END_TYPE
