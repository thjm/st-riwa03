(*
   Project: RiWa03_4 - 20140925
   
   Author:  HJM
   
   Features: - Steuerung fuer Kellerabgang (2 Taster & 2 Lampen)
             - Wohnzimmerlicht (2 Taster (Rest parallel) & 2 Lampen)
             - Diagnostik f�r Einspeiseklemme 24V
             - 2 Lampen auf Terasse
             - F_IS_ADVENT() & F_IS_NIGHT3()
             - SET_RTC()
             - F_GET_SUNRISE() & F_GET_SUNSET()
             - 1 mit auto-off tags�ber
             - Steckdosen �ber Taster
             - Steckdose mit Zeituhr
             - Rolladen (Fenster) �ber Taster, Nachtschaltung
             - Rolladen (T�r) �ber Taster
             - Debug-Variablen im Merker-Bereich
   
   $Id: RiWa03_5.st 20934 2015-04-26 16:58:30Z mathes $
 *)

PROGRAM MAIN
VAR
	(* --- WZ: 2 Deckenleuchten --- *)

	fbSWITCH_P : SWITCH_I; (* OSCAT building lib 100, FUNCTION block SWITCH_I *)
	fbSWITCH_S : SWITCH_I;

(* --- WZ: Rolladensteuerung, Fenster --- *)

    fbBLIND_INPUT_WINDOW : BLIND_INPUT;
	fbBLIND_NIGHT_WINDOW : BLIND_NIGHT;
    fbBLIND_X_WINDOW : BLIND_CONTROL_S;

	stBlindStatusWindow AT %MB0 : ST_BlindStatus;

(* --- WZ: Rolladensteuerung, Tuer --- *)

    fbBLIND_INPUT_DOOR : BLIND_INPUT;
(*	fbBLIND_NIGHT_DOOR : BLIND_NIGHT; *)
	fbBLIND_X_DOOR : BLIND_CONTROL_S;

	stBlindStatusDoor AT %MB2 : ST_BlindStatus;

(* --- Terasse: 2 Taster und 2 Lampen --- *)

 	fbSWITCH_T1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_T2 : SWITCH_I;

	(* --- 3 Taster und 2 Lampen am Kellerabgang --- *)

	fbDUAL_LAMP : FB_DUAL_LAMP; (* from my own function blocks *)

(* --- Steckdosen --- *)

(*
	fbSteckdose1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
	fbSteckdose2 : SWITCH_I;
*)
	fbSteckdose3 : SWITCH_I;
	fbSteckdose4 : SWITCH_I;
	fbSteckdose5 : SWITCH_I;
	fbSteckdose6 : SWITCH_I;
	fbSteckdose7 : SWITCH_I;
	fbSteckdose8 : SWITCH_I;
	fbSteckdose9 : SWITCH_I;
	fbSteckdose10 : SWITCH_I;

	fbSteckdoseGarten : SWITCH_I;

(* - Funktionsblock RTC und Interface zum Stellen der RTC - *)

	stSPSmailbox AT %MB4 : ST_SPSMailbox; (* 2 Bytes Groesse *)
	stRTCData AT %MB6 : ST_RTCData; (* 8 Bytes, direkt im Anschluss an stSPSMailbox *)
	(*naechste freie Adresse %MB14 *)

	fbRTC : RTC;	(* - ben�tigt: Standard.lb6, TcPlcUtilitiesBC.lb6, PlcSystemBC.lb6 - *)

(*
	stSPSinCopy AT %MB16 : ST_SPSinVariables;
	stSPSoutCopy AT %MB20 : ST_SPSoutVariables;
*)

	bNacht : BOOL := FALSE; (* ist nachts TRUE *)
	bAdvent : BOOL; (* ist im Advent TRUE, fuer Adventsbeleuchtung *)

	todSunRise : TOD; (* lokale Zeit des Sonnenaufgangs *)
	todSunSet : TOD; (* lokale Zeit des Sonnenuntergangs *)

	fbTimer : TIMER_1;  (* OSCAT building lib 100, function block TIMER_1 *)

	bFirst : BOOL := TRUE;     (* zeigt den 1. Zyklus der SPS an *)

END_VAR

(* - einmalig, zu Programmbeginn - *)

IF bFirst THEN
	bFirst := FALSE;

	(* Echtzeituhr: stellen = PRESET *)
	fbRTC(EN := TRUE, PDT := DT#2014-09-25-15:00:00);
END_IF

(* - Input-Variablen in den Merker-Bereich kopieren - *)

(*
MEMCPY(ADR(stSPSinCopy), ADR(stSPSinVar),SIZEOF(ST_SPSinVariables));
*)

(* - RTC ggf. stellen (FB) und Zeit weiterzaehlen - *)

SET_RTC(ADR(stSPSmailbox), ADR(stRTCData), ADR(fbRTC));

fbRTC(EN := FALSE);   (* muss einmal pro PLC-Zyklus gerufen werden *)

(* - Zeit fuer Sonnenaufgang/-untergang bestimmen - *)

todSunRise := F_GET_SUNRISE(fbRTC.CDT);
todSunSet := F_GET_SUNSET(fbRTC.CDT);

(* - feststellen, ob es Nacht ist - *)

(* bNacht := F_IS_NIGHT(fbRTC.CDT); *)
bNacht := F_IS_NIGHT3(fbRTC.CDT, todSunRise, todSunSet);

(* - WZ: Schema f�r 'Hauptbeleuchtung (ex. Couchtisch)' - *)

fbSWITCH_P(IN := stSPSinVar.bTASTER_P1 OR stSPSinVar.bTASTER_S1, T_DEBOUNCE := t#20ms );

stSPSoutVar.bLampe1 := fbSWITCH_P.Q;

(* - WZ: Schema f�r  'Nebenbeleuchtung (ex. Esstisch)' - *)

fbSWITCH_S(IN := stSPSinVar.bTASTER_P2 OR stSPSinVar.bTASTER_S2, T_DEBOUNCE := t#20ms );

stSPSoutVar.bLampe2 := fbSWITCH_S.Q;

(* - Schema fuer Rolladen, Fenster - *)

fbBLIND_INPUT_WINDOW(S1 := stSPSinVar.bTASTER_BLIND_WINDOW_UP,
			S2 := stSPSinVar.bTASTER_BLIND_WINDOW_DOWN,
            IN := FALSE,
(* TRUE fuer Single-Click Mode *)
            CLICK_EN := TRUE,
(* Timeout fuer Click-Erkennung *)
			CLICK_TIME := t#300ms,
            pos := fbBLIND_X_WINDOW.POS,
(* Position wenn IN == TRUE *)
            pi := 16#FF,
(* Timeout fuer Handbetrieb *)
            MANUAL_TIMEOUT := t#2m,
(* Timeout fuer eine Bewegung *)
            MAX_RUNTIME := t#20s,
(* zur�ck zur alten Position wenn Zwang weg *)
			MASTER_MODE := TRUE);

fbBLIND_NIGHT_WINDOW(UP := fbBLIND_INPUT_WINDOW.QU,
            DN := fbBLIND_INPUT_WINDOW.QD,
            S_IN := fbBLIND_INPUT_WINDOW.STATUS,
			pi := fbBLIND_INPUT_WINDOW.PO,
(* Tagschaltung aktiv/inaktive *)
			E_DAY := TRUE,
(* Nachtschaltung aktiv/inaktiv *)
			E_NIGHT := TRUE,
(* aktuelle Zeit/Datum *)
			DTIN := fbRTC.CDT,
(* Sonnenaufgangszeit *)
			SUNRISE := todSunRise,
(* Offset von Sonnenaufgang [Minuten] *)
			SUNRISE_OFFSET := tSunRiseOffset,
(* Sonnenuntergangszeit *)
			SUNSET := todSunSet,
(* Offset vom Sonnenuntergang [Minuten] *)
			SUNSET_OFFSET := tSunSetOffset,
(* Position Nacht *)
			NIGHT_POSITION := 16#60);

fbBLIND_X_WINDOW(UP := fbBLIND_NIGHT_WINDOW.QU,
            DN := fbBLIND_NIGHT_WINDOW.QD,
            S_IN := fbBLIND_NIGHT_WINDOW.STATUS,
            pi := fbBLIND_NIGHT_WINDOW.PO,
            T_LOCKOUT := t#200ms,
(* Verlaengerungszeit bei Endanschlag, benoetigt EXT_TRG zur Aktivierung *)
            T_EXT := t#2s,
(* Laufzeit des Rollos, Richtung  'UP' *)
            T_UP := t#15s,
(* Laufzeit des Rollos, Richtung 'DOWN' *)
            T_DN := t#15s);

stSPSoutVar.bMOTOR_BLIND_WINDOW_UP := fbBLIND_X_WINDOW.MU;
stSPSoutVar.bMOTOR_BLIND_WINDOW_DOWN := fbBLIND_X_WINDOW.MD;

stBlindStatusWindow.Position := fbBLIND_X_WINDOW.POS;
stBlindStatusWindow.Status := fbBLIND_X_WINDOW.STATUS;

(* - Schema fuer Rolladen, Tuer - *)

fbBLIND_INPUT_DOOR(S1 := stSPSinVar.bTASTER_BLIND_DOOR_UP,
			S2 := stSPSinVar.bTASTER_BLIND_DOOR_DOWN,
            IN := FALSE,
(* TRUE fuer Single-Click Mode *)
            CLICK_EN := TRUE,
(* Timeout fuer Click-Erkennung *)
			CLICK_TIME := t#300ms,
            pos := fbBLIND_X_DOOR.POS,
(* Position wenn IN == TRUE *)
            pi := 16#FF,
(* Timeout fuer Handbetrieb *)
            MANUAL_TIMEOUT := t#2m,
(* Timeout fuer eine Bewegung *)
            MAX_RUNTIME := t#24s);

fbBLIND_X_DOOR(UP := fbBLIND_INPUT_DOOR.QU,
            DN := fbBLIND_INPUT_DOOR.QD,
            S_IN := fbBLIND_INPUT_DOOR.STATUS,
            pi := fbBLIND_INPUT_DOOR.PO,
            T_LOCKOUT := t#200ms,
(* Verlaengerungszeit bei Endanschlag, benoetigt EXT_TRG zur Aktivierung *)
            T_EXT := t#2s,
(* Laufzeit des Rollos, Richtung  'UP' *)
            T_UP := t#22s,
(* Laufzeit des Rollos, Richtung 'DOWN' *)
            T_DN := t#22s);

stSPSoutVar.bMOTOR_BLIND_DOOR_UP := fbBLIND_X_DOOR.MU;
stSPSoutVar.bMOTOR_BLIND_DOOR_DOWN := fbBLIND_X_DOOR.MD;

stBlindStatusDoor.Position := fbBLIND_X_DOOR.POS;
stBlindStatusDoor.Status := fbBLIND_X_DOOR.STATUS;

(* - WZ: Schema fuer 10 Steckdosen, bSteckdose5 nicht beschaltet - *)

(*
fbSteckdose1(IN := stSPSinVar.bTASTER_P4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose1 := fbSteckdose1.Q; (* an Eingangstuer *)
stSPSoutVar.bSteckdose2 := fbSteckdose1.Q; (* an Durchgangstuer *)
*)
stSPSoutVar.bSteckdose1 := TRUE; (* an Eingangstuer, permanent an *)
stSPSoutVar.bSteckdose2 := TRUE; (* an Durchgangstuer, permanent an *)

(*
fbSteckdose2(IN := stSPSinVar.bTASTER_S3, T_DEBOUNCE := t#20ms );
bSteckdose2 := fbSteckdose2.Q; (* an Durchgangstuer *)
*)

fbSteckdose3(IN := stSPSinVar.bTASTER_P4 OR stSPSinVar.bTASTER_S4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose3 := fbSteckdose3.Q; (* in Ecke bei Durchgangstuer, fuer Schrankbeleuchtung *)

fbSteckdose4(IN := stSPSinVar.bTASTER_G3, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose4 := fbSteckdose4.Q; (* in Ecke bei Terassentuer *)

fbSteckdose5(IN := FALSE, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose5 := fbSteckdose5.Q;

fbSteckdose6(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose6 := fbSteckdose6.Q;  (* in Ecke bei Terassentuer *)

(* Setze bAdvent im Dezember und bis zum 15. Januar (Funktion F_IS_ADVENT() *)

bAdvent := F_IS_ADVENT(fbRTC.CDT);

(* Anschalten mit Timer, benutzt Startzeit und Intervall.
   Alternative: Funktion TIMECHECK() *)
fbTIMER(E := bAdvent, DTI := fbRTC.CDT, START := tod#17:00:00, DURATION := t#7h00m, DAY := 2#0111_1111);
fbSteckdose7(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms );
IF bADVENT THEN
	stSPSoutVar.bSteckdose7 := fbTIMER.Q; (* Ecke, fuer Adventsbeleuchtung *)
ELSE
	stSPSoutVar.bSteckdose7 := fbSteckdose7.Q;
END_IF

fbSteckdose8(IN := stSPSinVar.bTASTER_G4, T_DEBOUNCE := t#20ms ); (* in Ecke, fuer ... *)
stSPSoutVar.bSteckdose8 := fbSteckdose8.Q;

fbSteckdose9(IN := stSPSinVar.bTASTER_P3 OR stSPSinVAR.bTASTER_S3, T_DEBOUNCE := t#20ms ); (* Multimediawand, linke Steckdose *)
stSPSoutVar.bSteckdose9 := fbSteckdose9.Q;

fbSteckdose10(IN := stSPSinVar.bTASTER_P3 OR stSPSinVAR.bTASTER_S3, T_DEBOUNCE := t#20ms ); (* Multimediawand, rechte Steckdose *)
stSPSoutVar.bSteckdose10 := fbSteckdose10.Q;

(* - Schemata fuer die Terasse (z.Zt. mit EG2/S3 verbunden) - *)

IF NOT bNacht THEN
	fbSWITCH_T1(IN := stSPSinVar.bTASTER_EG2 OR stSPSinVar.bTASTER_T1, T_on_max := t#60s, T_DEBOUNCE := t#20ms);
ELSE
	fbSWITCH_T1(IN := stSPSinVar.bTASTER_EG2 OR stSPSinVar.bTASTER_T1, T_on_max := t#0s, T_DEBOUNCE := t#20ms);
END_IF

stSPSoutVar.bLampe_T1 := fbSWITCH_T1.Q;

(* fuer beide Lampen evtl. Serienschaltung nehmen ... *)
fbSWITCH_T2(IN := stSPSinVar.bTASTER_T2, T_DEBOUNCE := t#20ms);

stSPSoutVar.bLampe_T2 := fbSWITCH_T2.Q;

(* Steckdose auf der Terasse *)
(*
fbSteckdoseGarten(IN := FALSE, T_DEBOUNCE := t#20ms );
stSPSoutVar.bSteckdose11 := fbSteckdoseGarten.Q; (* Steckdose auf Terasse *)
*)
stSPSoutVar.bSteckdose11 := TRUE; (* Steckdose auf Terasser, permanent an *)

(* - Schema f�r 'Kellerabgang', keine Debug-Ausgabe - *)

fbDUAL_LAMP(in1 := stSPSinVar.bTASTER_EG1,
		in2 := stSPSinVar.bTASTER_KG,
		in3 := FALSE,
        use_timer := TRUE,
		T_double_wait := t#3000ms );

stSPSoutVar.bLampe_EG := fbDUAL_LAMP.out1;
stSPSoutVar.bLampe_KG := fbDUAL_LAMP.out2;

(* - Output-Variablen in den Merker-Bereich kopieren - *)

(*
MEMCPY(ADR(stSPSoutCopy),ADR(stSPSoutVar),SIZEOF(ST_SPSoutVariables));
*)

VAR_GLOBAL
	setup : CONSTANTS_SETUP;

	stSPSinVar AT %IB0 : ST_SPSinVariables;
	stSPSoutVar AT %QB0 : ST_SPSoutVariables;

	tSunRiseOffset : TIME := t#0m;
	tSunSetOffset : TIME := t#30m;

	sunRiseConst : ARRAY[1..366] OF INT :=

		442, 442, 442, 442, 442, 441, 441, 441, 440, 440, 439, 439, 438, 437, 
		437, 436, 435, 434, 434, 433, 432, 431, 430, 429, 428, 426, 425, 424, 
		423, 421, 420, 419, 417, 416, 415, 413, 412, 410, 408, 407, 405, 404, 
		402, 400, 399, 397, 395, 393, 391, 390, 388, 386, 384, 382, 380, 378, 
		376, 374, 372, 371, 369, 367, 365, 363, 360, 358, 356, 354, 352, 350,
		348, 346, 344, 342, 340, 338, 336, 333, 331, 329, 327, 325, 323, 321, 
		319, 317, 314, 312, 310, 308, 306, 304, 302, 300, 298, 296, 294, 292, 
		289, 287, 285, 283, 281, 279, 277, 275, 273, 271, 270, 268, 266, 264, 
		262, 260, 258, 256, 255, 253, 251, 249, 248, 246, 244, 243, 241, 239, 
		238, 236, 235, 233, 232, 230, 229, 227, 226, 225, 224, 222, 221, 220, 
		219, 218, 217, 215, 214, 214, 213, 212, 211, 210, 209, 209, 208, 207, 
		207, 206, 206, 205, 205, 204, 204, 204, 204, 203, 203, 203, 203, 203,
		203, 203, 203, 204, 204, 204, 204, 205, 205, 206, 206, 207, 207, 208, 
		208, 209, 210, 211, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 
		221, 222, 223, 224, 225, 227, 228, 229, 230, 231, 233, 234, 235, 237, 
		238, 239, 241, 242, 243, 245, 246, 247, 249, 250, 252, 253, 254, 256, 
		257, 259, 260, 261, 263, 264, 266, 267, 269, 270, 271, 273, 274, 276, 
		277, 279, 280, 281, 283, 284, 286, 287, 289, 290, 291, 293, 294, 296, 
		297, 298, 300, 301, 303, 304, 306, 307, 308, 310, 311, 313, 314, 316, 
		317, 319, 320, 321, 323, 324, 326, 327, 329, 330, 332, 333, 335, 336, 
		338, 339, 341, 342, 344, 345, 347, 348, 350, 351, 353, 354, 356, 358, 
		359, 361, 362, 364, 365, 367, 369, 370, 372, 373, 375, 377, 378, 380, 
		381, 383, 385, 386, 388, 389, 391, 392, 394, 396, 397, 399, 400, 402,
		403, 405, 406, 408, 409, 411, 412, 414, 415, 416, 418, 419, 420, 422, 
		423, 424, 425, 426, 428, 429, 430, 431, 432, 433, 434, 434, 435, 436, 
		437, 437, 438, 439, 439, 440, 440, 441, 441, 441, 442, 442, 442, 442, 
		442, 442;

	sunSetConst : ARRAY[1..366] OF INT :=

		941, 942, 943, 944, 945, 947, 948, 949, 950, 952, 953, 954, 956, 957, 
		958, 960, 961, 963, 964, 966, 967, 969, 970, 972, 974, 975, 977, 978, 
		980, 982, 983, 985, 987, 988, 990, 992, 993, 995, 997, 998, 1000, 1002, 
		1003, 1005, 1007, 1008, 1010, 1012, 1013, 1015, 1016, 1018, 1020, 1021, 1023, 1025, 
		1026, 1028, 1029, 1031, 1033, 1034, 1036, 1037, 1039, 1040, 1042, 1044, 1045, 1047, 
		1048, 1050, 1051, 1053, 1054, 1056, 1058, 1059, 1061, 1062, 1064, 1065, 1067, 1068,
		1070, 1071, 1073, 1074, 1076, 1077, 1079, 1080, 1082, 1083, 1085, 1086, 1088, 1089, 
		1091, 1092, 1094, 1095, 1097, 1098, 1100, 1101, 1103, 1104, 1106, 1107, 1109, 1110, 
		1112, 1113, 1115, 1116, 1118, 1119, 1121, 1122, 1124, 1125, 1127, 1128, 1129, 1131, 
		1132, 1134, 1135, 1137, 1138, 1139, 1141, 1142, 1143, 1145, 1146, 1147, 1149, 1150, 
		1151, 1152, 1154, 1155, 1156, 1157, 1158, 1159, 1161, 1162, 1163, 1164, 1165, 1165, 
		1166, 1167, 1168, 1169, 1170, 1170, 1171, 1172, 1172, 1173, 1173, 1174, 1174, 1175, 
		1175, 1175, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 1176, 
		1175, 1175, 1175, 1174, 1174, 1173, 1173, 1172, 1171, 1171, 1170, 1169, 1168, 1168, 
		1167, 1166, 1165, 1164, 1163, 1162, 1160, 1159, 1158, 1157, 1156, 1154, 1153, 1151, 
		1150, 1149, 1147, 1146, 1144, 1143, 1141, 1139, 1138, 1136, 1135, 1133, 1131, 1129, 
		1128, 1126, 1124, 1122, 1120, 1118, 1117, 1115, 1113, 1111, 1109, 1107, 1105, 1103,
		1101, 1099, 1097, 1095, 1093, 1091, 1089, 1087, 1085, 1083, 1080, 1078, 1076, 1074, 
		1072, 1070, 1068, 1066, 1064, 1061, 1059, 1057, 1055, 1053, 1051, 1049, 1046, 1044, 
		1042, 1040, 1038, 1036, 1034, 1031, 1029, 1027, 1025, 1023, 1021, 1019, 1017, 1015, 
		1013, 1011, 1009, 1007, 1005, 1003, 1001, 999, 997, 995, 993, 991, 989, 987, 
		985, 983, 981, 980, 978, 976, 974, 973, 971, 969, 968, 966, 964, 963, 
		961, 960, 958, 957, 955, 954, 953, 951, 950, 949, 947, 946, 945, 944, 
		943, 942, 941, 940, 939, 938, 937, 937, 936, 935, 934, 934, 933, 933, 
		932, 932, 932, 931, 931, 931, 931, 931, 931, 931, 931, 931, 931, 931, 
		931, 932, 932, 932, 933, 933, 934, 935, 935, 936, 937, 937, 938, 939, 
		940, 940;

END_VAR
