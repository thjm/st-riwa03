(*
   Project: RiWa03
   
   Author:  HJM
   
   Purpose: ST Function SET_RTC
   
   $Id: SetRtc.st 19389 2014-10-15 20:57:38Z mathes $
 *)

TYPE E_SPSCommand :
	(kNone,
	 kSetTime) := kNone;
END_TYPE

TYPE E_SPSStatus :
	(kOK, kCommandError) := kOK;
END_TYPE

TYPE ST_BlindStatus :
STRUCT
	Status : BYTE; (* ESR compatible status byte *)
	Position : BYTE; (* current position of the blind *)
	(* Angle : BYTE; *)
END_STRUCT
END_TYPE

TYPE ST_RTCData :
STRUCT
	setTime : DWORD;
	currentTime : DWORD;
END_STRUCT
END_TYPE

TYPE ST_SPSMailbox :
STRUCT
	Command : E_SPSCommand;	(* command register *)
	Status : E_SPSStatus;	(* status register *)
END_STRUCT
END_TYPE

FUNCTION SET_RTC : BOOL
VAR_INPUT
	stSPSmailbox : POINTER TO ST_SPSMailbox;
	stRTCData : POINTER TO ST_RTCData;
	fbRTC : POINTER TO RTC;
END_VAR
VAR
	setTime : DATE_AND_TIME;
	status : BOOL := FALSE;
END_VAR

IF stSPSmailbox^.Command <>  kNone THEN
	CASE stSPSmailbox^.Command OF
		kSetTime :
			setTime := DWORD_TO_DT( stRTCData^.setTime );
		    stSPSmailbox^.Status := kOK;
			status := TRUE;
	ELSE
		    stSPSmailbox^.Status := kCommandError;
	END_CASE

	stSPSmailbox^.Command := kNone; (* reset command byte *)
END_IF

IF status THEN
	fbRTC^(EN := TRUE, PDT := setTime);
END_IF

stRTCData^.currentTime := DT_TO_DWORD(fbRTC^.CDT);

SET_RTC := status;
