(* $Id: ST_SPSinVariables.st 20872 2015-03-31 18:38:49Z mathes $ *)

TYPE ST_SPSInVariables :
STRUCT
	(* - KL1408 an Position 2, Reserve - *)

	bTASTER_EG2 AT %IX0.0 : BOOL; (* dummy, not connected *)
	bIN_0_1 AT %IX0.1 : BOOL;
	bIN_0_2 AT %IX0.2 : BOOL;
	bIN_0_3 AT %IX0.3 : BOOL;
	bIN_0_4 AT %IX0.4 : BOOL;
	bIN_0_5 AT %IX0.5 : BOOL;
	bIN_0_6 AT %IX0.6 : BOOL;
	bIN_0_7 AT %IX0.7 : BOOL;

	(* - KL1104 an Position 3, 2 Koppelrelais - *)

	bTASTER_P1 AT %IX1.0 : BOOL;
	bTASTER_P2 AT %IX1.1 : BOOL;
	bTASTER_P3 AT %IX1.2 : BOOL;
	bTASTER_P4 AT %IX1.3 : BOOL;

	(* - KL1104 an Position 4 - *)

	bTASTER_S1 AT %IX1.4 : BOOL;
	bTASTER_S2 AT %IX1.5 : BOOL;
	bTASTER_S3 AT %IX1.6 : BOOL;
	bTASTER_S4 AT %IX1.7 : BOOL;

	(* - KL1104 an Position 5 - *)

	bIN_1_4 AT %IX2.0 : BOOL;
	bIN_1_5 AT %IX2.1 : BOOL;
	bTASTER_T1 AT %IX2.2 : BOOL;
	bTASTER_T2 AT %IX2.3 : BOOL;

	(* - KL1104 an Position 6 - *)

	bTASTER_G1 AT %IX2.4 : BOOL;
	bTASTER_G2 AT %IX2.5 : BOOL;
	bTASTER_G3 AT %IX2.6 : BOOL;
	bTASTER_G4 AT %IX2.7 : BOOL;

(* - 750-609 (mit Diagnostik) an Position 7 - *)

	b24VPowerGood AT %IX3.0 : BOOL;
	b24VFuseFail AT %IX3.1 : BOOL;

(* - KL2134 an Position 8 - *)
(* - KL2134 an Position 9 - *)
(* - KL2134 an Position 10 - *)

(* - Tbd: KL6180 (Trennklemme) an Position ?? - *)

(* - KL9150 ... / 750-611 an Position 11 - *)

(* - KL1702 / 750-405 an Position 12 - *)

	bTASTER_EG1 AT %IX3.2 : BOOL;
	bTASTER_KG AT %IX3.3 : BOOL;

(* - KL2602 / 750-512 an Position 13 - *)
(* - KL9020 (K-Bus-Verlängerung) an Position 14 - *)
(* - KL9050 (K-Bus-Verlängerung) an Position 15 - *)

	(* - KL9150 ... / 750-611 an Positin 16 - *)
	(* - KL2602 / 750-512 an Position 17 - *)
	(* - KL2602 / 750-512 an Position 18 - *)

(* - KL1702 / 750-405 an Position 19 - *)

	bTASTER_BLIND_WINDOW_UP AT %IX3.4 : BOOL;
	bTASTER_BLIND_WINDOW_DOWN AT %IX3.5 : BOOL;

	(* - KL2602 / 750-512 an Position 20 - *)

(* - KL1702 / 750-405 an Position 21 - *)

	bTASTER_BLIND_DOOR_UP AT %IX3.6 : BOOL;
	bTASTER_BLIND_DOOR_DOWN AT %IX3.7 : BOOL;

	(* - KL2602 / 750-512 an Position 22 - *)

(* - KL9010 (Endklemme) an Position 23 - *)

END_STRUCT
END_TYPE