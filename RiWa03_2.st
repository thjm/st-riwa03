(*
   Project: RiWa03_2 - 20130925
   
   Author:  HJM
   
   Features: - Steuerung fuer Kellerabgang (2 Taster & 2 Lampen)
             - Wohnzimmerlicht (2*2 Taster & 2 Lampen)
             - Diagnostik fuer Einspeiseklemme 24V
             - Ausgaenge fuer Steckdosen reserviert
   
   $Id: RiWa03_2.st 17610 2014-01-18 08:27:23Z mathes $
 *)
 
 
PROGRAM MAIN
VAR

(* --- WZ: 4 Taster bei Eingangst�r (P)  --- *)

	bTASTER_P1 AT %I* : BOOL;
	bTASTER_P2 AT %I* : BOOL;
	bTASTER_P3 AT %I* : BOOL;
	bTASTER_P4 AT %I* : BOOL;

(* --- WZ: 3 Taster bei Durchgangst�r (S)  --- *)

	bTASTER_S1 AT %I* : BOOL;
	bTASTER_S2 AT %I* : BOOL;
	bTASTER_S3 AT %I* : BOOL;
	bTASTER_S4 AT %I* : BOOL;

(* --- WZ: 2 Deckenleuchten --- *)

 	bLAMP1 AT %Q* : BOOL;
 	bLAMP2 AT %Q* : BOOL;

 	fbSWITCH_P : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_S : SWITCH_I;

(* --- KL2134, noch nicht benutzt --- *)

	bOut0 AT %Q* : BOOL;
	bOut1 AT %Q* : BOOL;
	bOut2 AT %Q* : BOOL;
	bOut3 AT %Q* : BOOL;

(* --- KL2134, noch nicht benutzt --- *)

	bOut4 AT %Q* : BOOL;
	bOut5 AT %Q* : BOOL;
	bOut6 AT %Q* : BOOL;
	bOut7 AT %Q* : BOOL;

(* --- Diagnostic fuer Einspeiseklemme --- *)

	bPowerGood AT %I* : BOOL;
	bFuseFail AT %I* : BOOL;

(* --- WZ: 4 Taster an Gartentuer --- *)
^
(*
	bTASTER_G1 AT %I* : BOOL;
	bTASTER_G2 AT %I* : BOOL;
	bTASTER_G3 AT %I* : BOOL;
	bTASTER_G4 AT %I* : BOOL;
*)

(* --- Terasse: 2 Taster und 2 Lampen --- *)

(*
	bTASTER_T1 AT %I* : BOOL;
	bTASTER_T2 AT %I* : BOOL;
	
	bLAMPE_T1 AT %I* : BOOL;
	bLAMPE_T2 AT %I* : BOOL;

 	fbSWITCH_T1 : SWITCH_I;  (* OSCAT building lib 100, function block SWITCH_I *)
 	fbSWITCH_T2 : SWITCH_I;
*)

(* --- Keller: 2 Taster und 2 Lampen am Kellerabgang --- *)

	bTASTER_EG1 AT %I* : BOOL;
	bTASTER_EG2 AT %I* : BOOL;
 	bTASTER_KG AT %I* : BOOL;
 	bLAMPE_EG AT %Q* : BOOL;
 	bLAMPE_KG AT %Q* : BOOL;
 
 	fbDUAL_LAMP : FB_DUAL_LAMP; (* from my own function blocks *)

(* --- Diagnostic fuer Einspeiseklemme --- *)

	bPowerGood AT %I* : BOOL;
	bFuseFail AT %I* : BOOL;

(* --- Echtzeituhr, ben�tigte Libs:  Standard.lb6, TcPlcUtilitiesBC.lb6, PlcSystemBC.lb6 --- *)

 	fbRTC : RTC;

 	bFirst : BOOL := TRUE;
END_VAR

VAR_CONFIG

(* --- KL1104 an Position 1, 2 x Koppelrelais --- *)

	main.bTASTER_EG1 AT %IX0.0 : BOOL;
	main.bTASTER_KG AT %IX0.1 : BOOL;
	main.bTASTER_EG2 AT %IX0.2 : BOOL;
	(* main.DUMMY at %IX0.3 . bool; *)

(* --- KL1104 an Position 2, 2 x Koppelrelais --- *)

	main.bTASTER_P1 AT %IX0.4 : BOOL;
	main.bTASTER_P2 AT %IX0.5 : BOOL;
	main.bTASTER_P3 AT %IX0.6 : BOOL;
	main.bTASTER_P4 AT %IX0.7 : BOOL;

(* --- KL1104 an Position 3 --- *)

	main.bTASTER_S1 AT %IX1.0 : BOOL;
	main.bTASTER_S2 AT %IX1.1 : BOOL;
	main.bTASTER_S3 AT %IX1.2 : BOOL;
	main.bTASTER_S4 AT %IX1.3 : BOOL;

(* --- KL1104 an Position 4 --- *)
(*
	main.bTASTER_G1 AT %IX1.4 : BOOL;
	main.bTASTER_G2 AT %IX1.5 : BOOL;
	main.bTASTER_G3 AT %IX1.6 : BOOL;
	main.bTASTER_G4 AT %IX1.7 : BOOL;
*)

(* --- KL1104 an Position 5 --- *)
(*
	main.bTASTER_T1 AT %IX2.0 : BOOL;
	main.bTASTER_T2 AT %IX2.1 : BOOL;
	main.bTASTER_T3 AT %IX2.2 : BOOL;
	main.bTASTER_T4 AT %IX2.3 : BOOL;
*)

(* --- KL1104 an Position 6 --- *)
(*
	main.bTASTER_S1 AT %IX2.4 : BOOL;
	main.bTASTER_S2 AT %IX2.5 : BOOL;
	main.bTASTER_S3 AT %IX2.6 : BOOL;
	main.bTASTER_S4 AT %IX2.7 : BOOL;
*)

(* --- 750-609 (mit Diagnostik) an Position 7 --- *)

	main.bPowerGood AT %IX3.0 : BOOL;
	main.bFuseFail AT %IX3.1 : BOOL;

(* --- KL2134 an Position 8 --- *)

	main.bOut0 AT %QX0.0 : BOOL;
	main.bOut1 AT %QX0.1 : BOOL;
	main.bOut2 AT %QX0.2 : BOOL;
	main.bOut3 AT %QX0.3 : BOOL;

(* --- KL2134 an Position 9 --- *)

	main.bOut4 AT %QX0.4 : BOOL;
	main.bOut5 AT %QX0.5 : BOOL;
	main.bOut6 AT %QX0.6 : BOOL;
	main.bOut7 AT %QX0.7 : BOOL;

(* --- KL9180, Trennklemme --- *)

(* --- KL9150 ... / 750-611 an Positon 11, Einspeisung Keller --- *)

(* --- KL2622 / 750-512 an Position 12 --- *)

	main.bLAMPE_EG AT %QX1.0 : BOOL;
	main.bLAMPE_KG AT %QX1.1 : BOOL;

(* --- KL9150 ... / 750-611 an Position 13, Einspeisung Wohnzimmer/Garten --- *)

(* --- KL2622 / 750-512 an Position 14 --- *)

	main.bLAMP1 AT %QX1.2 : BOOL;
	main.bLAMP2 AT %QX1.3 : BOOL;

(* --- KL2622 / 750-512 an Position 15 --- *)
(*
	main.bLAMP_G1 AT %QX1.4 : BOOL;
	main.bLAMP_G2 AT %QX1.5 : BOOL;
*)

(* --- KL2622 / 750-512 an Position 16 --- *)
(*
	main.bMOTOR_BLIND_WINDOW_UP AT %QX1.6 : BOOL;
	main.bMOTOR_BLIND_WINDOW_DOWN AT %QX1.7 : BOOL;
*)

(* --- KL2622 / 750-512 an Position 17 --- *)
(*
	main.bMOTOR_BLIND_DOOR_UP AT %QX2.0 : BOOL;
	main.bMOTOR_BLIND_DOOR_DOWN AT %QX2.1 : BOOL;
*)
END_VAR


(* --- einmalig, zu Programmbeginn --- *)

IF bFirst THEN
	bFirst := FALSE;

	(* Echtzeituhr: stellen = preset *)
	fbRTC(EN := TRUE, PDT := DT#2013-07-27-08:00:00);
END_IF

(* --- Zeit von RTC holen ... --- *)

fbRTC(EN := FALSE);

(* --- WZ: Schema f�r 'Hauptbeleuchtung (ex. Couchtisch)' --- *)

fbSWITCH_P(IN := bTASTER_P1 OR bTASTER_S1 (* OR bTASTER_G1 *), T_DEBOUNCE := t#20ms );

bLAMP1 := fbSWITCH_P.Q;

(* --- WZ: Schema f�r  'Nebenbeleuchtung (ex. Esstisch)' --- *)

fbSWITCH_S(IN := bTASTER_P2 OR bTASTER_S2, T_DEBOUNCE := t#20ms );

bLAMP2 := fbSWITCH_S.Q;

(* --- Schema f�r 'Kellerabgang', keine Debug-Ausgabe --- *)

fbDUAL_LAMP(in1 := bTASTER_EG1, in2 := bTASTER_KG, in3 := bTASTER_EG2,
             use_timer := TRUE, T_double_wait := t#3000ms );

bLAMPE_EG := fbDUAL_LAMP.out1;
bLAMPE_KG := fbDUAL_LAMP.out2;
