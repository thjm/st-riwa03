(*
   Project: RiWa03
   
   Author:  HJM
   
   Purpose: ST Function Block FB_DUAL_LAMP
   
   $Id: FBDualLamp.st 16596 2013-08-07 07:37:24Z mathes $
 *)
 

FUNCTION_BLOCK FB_DUAL_LAMP
VAR_INPUT
	in1 : BOOL;
	in2 : BOOL;
END_VAR
VAR_INPUT CONSTANT
	use_timer : BOOL := FALSE;
	T_double_wait : TIME := t#2000ms;
	T_long_wait : TIME := t#15s;
END_VAR
VAR_OUTPUT
	out1 : BOOL;
	out2 : BOOL;
END_VAR
VAR
	fbSWITCH1 : SWITCH_I; (* from OSCAT building lib 100 *)
	fbSWITCH2 : SWITCH_I;

	switch_state1 : BOOL := FALSE;
	switch_state2 : BOOL := FALSE;
	state_changed : BOOL := FALSE;  	            (* zeigt Zustandswechsel an *)
	state : BYTE := 0;

	(* for the timer extension 1 *)
	fbTON1 : TON;
	(* for the timer extension 2 *)
	fbTON2 : TON;
END_VAR

(* --- Serienschaltung mit 2 Tastern ---
	- Ruhezustand: state = 0
	- oberer Schalter:
		- 1. Tastendruck = Licht EG an, state = 1
		- 2. Tastendruck = Kellerlicht an, state = 2
		- 3. Tastendruck = Kellerlicht aus, state = 3
		- 4. Tastendruck = Licht EG aus, state = 0
	- unterer Schalter:
		- 1. Tastendruck = Kellerlicht an, state = 4
		- 2. Tastendruck = Licht EG an, state = 5
		- 3. Tastendruck = Licht EG aus, state = 6
		- 4. Tastendruck = Kellerlicht aus, state = 0
	- Erweiterung 1: Wenn nur eine Lampe an ist und man lange wartet, 
			 dann schaltet der n�chste Tastendruck sie wieder aus.
	- Erweiterung 2: Wenn mindestens eine Lampe an ist und man (sehr) lange
			 wartet, dann schaltet der n�chste Tastendruck sie (beide)
			 wieder aus.
*)

state_changed := FALSE;

(* - - - Erdgeschoss - - - *)

fbSWITCH1(IN := in1, T_DEBOUNCE := t#20ms);

(* bei steigender Flanke wird in den n�chsten Zustand geschaltet *)
IF  switch_state1 XOR fbSWITCH1.Q  THEN

	switch_state1 :=  fbSWITCH1.Q;  (* merke alten Zustand *)

	CASE state OF
		0 : state := 1  (* Off --> EG *);
		1 : state := 2; (* EG --> EG + KG *)
		2 : state := 3; (* EG + KG --> EG *)
		3 : state := 0; (* EG --> Off *)
		4 : state := 2; (* KG --> EG + KG *)
		5 : state := 3; (* KG + EG --> EG *)
		6 : state := 2; (* KG --> EG + KG  *)
	END_CASE;

	state_changed := TRUE;
END_IF;

(* - - - Kellergeschoss - - - *)

fbSWITCH2(IN := in2, T_DEBOUNCE := t#20ms);

IF  switch_state2 XOR fbSWITCH2.Q  THEN

	switch_state2 :=  fbSWITCH2.Q;  (* merke alten Zustand *)

	CASE state OF
		0 : state := 4; (* Off --> KG *)
		1 : state := 5; (* EG --> KG + EG *)
		2 : state := 6; (* KG + EG --> KG *)
		3 : state := 5; (* EG --> KG + EG *)
		4 : state := 5; (* KG --> KG + EG *)
		5 : state := 6; (* KG + EG --> KG *)
		6 : state := 0; (* KG --> Off *)
	END_CASE;

	state_changed := TRUE;
END_IF;

(* --- springe in anderen Zustand bei Doppelklickpause --- *)

IF use_timer THEN
	fbTON1(IN := (state = 1) OR (state = 4), pt := T_double_wait);

	IF fbTON1.q THEN
		CASE state OF
			1 : state := 3;
			4 : state := 6;
		END_CASE;
	END_IF;
END_IF;

(* --- 2. Timer starten, falls mindestens eine Lampe an --- *)

IF state_changed THEN
	fbTON2(IN := (state <> 0), pt := T_long_wait);
END_IF;

IF state_changed AND fbTON2.q THEN        (* nur bei Zustandswechsel *)
	state := 0;
	fbTON2(IN := FALSE); (* TON ruecksetzen, sonst wird immer ausgeschaltet *)
END_IF;

(* - - -  Auswertung und Lampen schalten  - - - *)

CASE state OF

	0 : out1 := FALSE;
	    out2 := FALSE;
	1 : out1 := TRUE;
	    out2 := FALSE;
	2 : out1 := TRUE;
	    out2 := TRUE;
	3 : out1 := TRUE;
	    out2 := FALSE;
	4 : out1 := FALSE;
	    out2 := TRUE;
	5 : out1 := TRUE;
	    out2 := TRUE;
	6 : out1 := FALSE;
	    out2 := TRUE;
END_CASE;
